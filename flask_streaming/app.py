#!/usr/bin/env python
from flask import Flask, render_template, Response

# emulated camera
# from camera import Camera

# Raspberry Pi camera module (requires picamera package)
from camera_pi import Camera
import time

app = Flask(__name__)
cam = Camera()

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

@app.route('/video')
def video():
    """Video streaming home page."""
    return render_template('video.html')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(cam),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

def gen(camera):
    """Video streaming generator function."""
    while True:
        time.sleep(0.1)
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/originvideo')
def originvideo():
    """Video streaming home page."""
    return render_template('originvideo.html')

@app.route('/origin_video_feed')
def origin_video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(origin_gen(cam),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

def origin_gen(camera):
    """Video streaming generator function."""
    while True:
        time.sleep(0.2)
        frame = camera.get_origin_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)

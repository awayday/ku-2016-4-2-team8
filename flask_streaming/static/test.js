var dData = function() {
        return Math.floor(Math.random() * 10);
};
var d = new Date();
var barData = {
        labels: [],
        datasets: [{
                label: "Number of passenger",
                fill: false,
                /*
                backgroundColor: ['rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'],
                */
                backgroundColor: 'rgba(0,00,71,1)',
                /*
                borderColor: ['rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'],
                */
                borderColor: 'rgba(193, 156, 114, 1)',
                //strokeColor: 'black',
                borderWidth: 2,
                data: []
        }]
}

var barDemo

function setDataInit(){
        $.ajax({ 
                url: "http://125.209.199.219/rest/currentnum.php?location_id=1", 
                success: function(data){
                        console.log(data);
                        barData['labels'].length = 0;
                        barData['datasets'][0]['data'].length = 0;

                        barData['labels'].push(data["max0"]['timestamp']);
                        barData['datasets'][0]['data'].push(data["max0"]['count']);

                        barData['labels'].push(data["max1"]['timestamp']);
                        barData['datasets'][0]['data'].push(data["max1"]['count']);

                        barData['labels'].push(data["max2"]['timestamp']);
                        barData['datasets'][0]['data'].push(data["max2"]['count']);

                        barData['labels'].push(data["max3"]['timestamp']);
                        barData['datasets'][0]['data'].push(data["max3"]['count']);

                        barData['labels'].push(data["max4"]['timestamp']);
                        barData['datasets'][0]['data'].push(data["max4"]['count']);

                        //////////////////////// 차트 생성 //////////////////////////////

                        var ctx = document.getElementById('chart').getContext('2d');
                        //var barDemo = new Chart(ctx).Bar(barData, {responsive: true});
                        // 위 코드 대신 아래 코드를 넣어야 잘 동작함.
                        barDemo = new Chart(ctx, {
                                //type: 'line',
                                type: 'bar',
                                data:barData,
                                responsive: true
                        });
                }, 
                dataType: "json"
        });
};

// 여기서부터 데이터 받아오는 부분
$(document).ready(function(){
        setDataInit();

        setInterval(function(){
                $.ajax({ url: "http://125.209.199.219/rest/currentnum.php?location_id=1", 
                success: function(data){
                        //Update your dashboard gauge
                        var html = '';

                        console.log(data)

                        //$.each(data, function(index, item){
                                /*
                                html += "<p>현재인원 : " + item["avg"] + "</p>";
                                var conse = (parseFloat(item["avg"])/10.0)*100.0
                                html += "<p>혼잡도 : " + conse + "%</p>";
                                */
                          //      var conse = (parseFloat(item["avg"])/10.0)*100.0

                            //    html += '<div class="input-group">';
                              //  html += '<input type="text" class="form-control" placeholder="현재인원 : '+ item["avg"] +'" aria-describedby="basic-addon2" readonly />';

                                //if(conse < 100){
                                  //      html += '<span class="input-group-addon green" id="basic-addon2">'+conse+'%</span>';
                                //} else if (100 <= conse < 150) {
                                 //       html += '<span class="input-group-addon yellow" id="basic-addon2">'+conse+'%</span>';
                                //} else {
                                 //       html += '<span class="input-group-addon red" id="basic-addon2">'+conse+'%</span>';
                                //}

                           //     html += '</div>';
                        //});

                        var conse = (parseFloat(data["max0"]['count'])/10.0)*100.0

                        html += '<div class="input-group">';
                        html += '<input type="text" class="form-control" placeholder="현재인원 : '+ data["max0"]['count'] +'" aria-describedby="basic-addon2" readonly />';

                        if(conse < 100){
                                html += '<span class="input-group-addon green" id="basic-addon2">'+conse+'%</span>';
                        } else if (100 <= conse < 150) {
                                html += '<span class="input-group-addon yellow" id="basic-addon2">'+conse+'%</span>';
                        } else {
                                html += '<span class="input-group-addon red" id="basic-addon2">'+conse+'%</span>';
                        }

                        html += '</div>';

                        console.log(d.getFullYear()+":"+(d.getMonth() + 1)+":"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());

                        if (barData['datasets'][0]['data'].length < 10){
                                barData['datasets'][0]['data'].push(data["max0"]['count']);
                                barData['labels'].push(data["max0"]['timestamp']);
                        } else {
                                barData['datasets'][0]['data'].shift();
                                barData['datasets'][0]['data'].push(data["max0"]['count']);
                                barData['labels'].shift();
                                barData['labels'].push(data["max0"]['timestamp']);

                        }
                        
                        // 아래는 테스트 코드임
                        //barData['datasets'][0]['data'].push(dData());
                        barDemo.update();

                        $("#ajaxReturn").html(html);
                }, dataType: "json"});
        }, 3000);
});